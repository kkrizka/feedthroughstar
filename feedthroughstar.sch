EESchema Schematic File Version 4
LIBS:feedthroughstar-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Feed Through for Star Chips"
Date "2019-06-19"
Rev "1"
Comp "Karol Krizka"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2000 1000 1200 600 
U 5CFCC065
F0 "Conn0" 50
F1 "SamePinsFeedthough.sch" 50
$EndSheet
$Sheet
S 2000 1800 1200 600 
U 5CFCC2A6
F0 "Conn1" 50
F1 "SamePinsFeedthough.sch" 50
$EndSheet
$Sheet
S 2000 2600 1200 600 
U 5CFCC32F
F0 "Conn2" 50
F1 "SamePinsFeedthough.sch" 50
$EndSheet
$Comp
L Mechanical:MountingHole H1
U 1 1 5CFBDF26
P 5000 1500
F 0 "H1" H 5100 1546 50  0000 L CNN
F 1 "MountingHole" H 5100 1455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5000 1500 50  0001 C CNN
F 3 "~" H 5000 1500 50  0001 C CNN
	1    5000 1500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5CFBE40C
P 5000 1700
F 0 "H2" H 5100 1746 50  0000 L CNN
F 1 "MountingHole" H 5100 1655 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5000 1700 50  0001 C CNN
F 3 "~" H 5000 1700 50  0001 C CNN
	1    5000 1700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5CFBE669
P 5700 1500
F 0 "H3" H 5800 1546 50  0000 L CNN
F 1 "MountingHole" H 5800 1455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5700 1500 50  0001 C CNN
F 3 "~" H 5700 1500 50  0001 C CNN
	1    5700 1500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5CFBE9C8
P 5700 1700
F 0 "H4" H 5800 1746 50  0000 L CNN
F 1 "MountingHole" H 5800 1655 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5700 1700 50  0001 C CNN
F 3 "~" H 5700 1700 50  0001 C CNN
	1    5700 1700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
